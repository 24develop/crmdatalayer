﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachinesLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            Machine builder = new Machine();
            Machine trooper = new Machine();
            Machine helicopter = new Machine();

            builder.MachineID = 0;
            trooper.MachineID = 1;
            helicopter.MachineID = 2;
            builder.Name = "Кран";
            trooper.Name = "Экскаватор";
            helicopter.Name = "Вертолет";

            List<Machine> MachineList = new List<Machine>();
            MachineList.Add(builder);
            MachineList.Add(trooper);
            MachineList.Add(helicopter);
        }
    }

    interface IMachineGetter
    {
        public IEnumerable<Machine> GetMachines();
        public IEnumerable<Machine> GetMachines(string category);

        public Machine GetMashine(int id);

        void SaveOrUpdate(Machine machine);
    }

    public class MachineGetter : IMachineGetter
    {
        public List<Machine> _machines = new List<Machine>();
        public MachineGetter(List<Machine> machines)
        {
            _machines = machines;
        }
        public Machine GetMachines(string Category)
        {
            throw new NotImplementedException();
        }
        public IEnumerable<Machine> GetMachines()
        {

            throw new NotImplementedException();
        }
        public IEnumerable<Machine> GetMachines(string machineCategory)
        {
            throw new NotImplementedException();
        }


    }

    public interface IMachine
    {
        public bool isAvailable { get; set; }
    }

    public class Machine : IMachine
    {
        public int MachineID { get; set; }

        public MachineDrivers Driver { get; set; }
        public MachineOwners Owner { get; set; }

        public string Number { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string Info { get; set; }
        public string Note { get; set; }

    }

    interface IContractor
    {
    }
    #region stubs
    public class MachineDrivers : IContractor
    {

    }
    public class MachineOwners : IContractor
    {

    }
    #endregion
}
